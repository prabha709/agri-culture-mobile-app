import 'dart:io';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class AddAddressScreen extends StatefulWidget {
  final userToken;

  AddAddressScreen({this.userToken});
  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

enum SingingCharacter { Home, Work }

class _AddAddressScreenState extends State<AddAddressScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  String _address1;
  String _address2;
  String _pincode;
String _addressType;
  bool loading = false;
  insertAddress() async {
    String url = "https://api-agriculture.datavivservers.in/ShippingAddress/";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();

      try {
        Map data = {
          "user_id": widget.userToken["userId"].toString(),
          "loc_latitude": "11",
          "loc_lonitude": "44",
          "address1": _address1,
          "address2": _address2,
          "address_category": _addressType,
          "pincode":_pincode
        };
        var response = await http.post("$url",
            headers: {"Authorization": "Token ${widget.userToken["token"]}"},
            body: data);

        if (response.statusCode == 200) {
          var jsonObject = json.decode(response.body);
          Navigator.pop(context);
            

        } else {
           var jsonObject = json.decode(response.body);
          return Flushbar(
              message: "$jsonObject",
              duration: Duration(seconds: 3),
              flushbarPosition: FlushbarPosition.BOTTOM
              // flushbarStyle: FlushbarStyle.FLOATING,
              )
            ..show(context);
        }
      } on SocketException catch (error) {
        return Flushbar(
          message:error.toString(),

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } catch (error) {
        return Flushbar(
          message: error.toString(),
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Address'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Fetch address form maps
              Container(
                padding: EdgeInsets.all(8),
                margin: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(8)),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.location_on),
                      Text(
                        'Use My Current Location',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              Form(
                key: _formkey,
                child: Padding(
                  padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                  child: Column(
                    children: [
                      TextFormField(
                        enabled: false,
                        decoration: InputDecoration(hintText: "latitude"),
                      ),
                      TextFormField(
                        enabled: false,
                        decoration: InputDecoration(hintText: "longtitude"),
                      ),
                      TextFormField(
                        decoration: InputDecoration(hintText: "Address1"),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "This field is Required";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (value)=>_address1 = value,
                      ),
                      TextFormField(
                        decoration: InputDecoration(hintText: "Address2"),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "This field is Required";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (value)=>_address2 = value,
                      ),
                      TextFormField(
                        decoration: InputDecoration(hintText: "pincode"),
                        keyboardType: TextInputType.phone,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "This field is Required";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (value)=>_pincode = value,
                      ),
                    ],
                  ),
                ),
              ),

              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text("Address Type"),
                  ),
                  RadioListTile(
                    value:"home",
                    title: Text("Home"), 
                    groupValue: _addressType, 
                    onChanged: (value){
                      setState(() {
                        _addressType= value;
                      });
                      print(_addressType);
                    }),
                     RadioListTile(
                    value:"work",
                    title: Text("Work"), 
                    groupValue: _addressType, 
                    onChanged: (value){
                      setState(() {
                        _addressType= value;
                      });
                      print(_addressType);
                    })
                  // ListTile(
                  //   title: const Text('Home'),
                  //   leading: Radio(
                  //     value: SingingCharacter.Home,
                  //     groupValue: _addressType,
                  //     onChanged: (SingingCharacter value) {
                  //       setState(() {
                  //         _addressType = value;
                  //       });
                  //     },
                  //   ),
                  // ),
                  // ListTile(
                  //   title: const Text('Work'),
                  //   leading: Radio(
                  //     value: SingingCharacter.Work,
                  //     groupValue: _addressType,
                  //     onChanged: (SingingCharacter value) {
                  //       setState(() {
                  //         _addressType = value;
                  //       });
                  //     },
                  //   ),
                  // ),
                ],
              ),

              //Save Button
              FlatButton(
                onPressed: () {
                  insertAddress();
                },
                
                color: Theme.of(context).accentColor,
                child: Text('Save',
                    style: TextStyle(color: Colors.white, fontSize: 28)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
