
import 'package:flutter/material.dart';

class ForgotPasswordScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(height: 200),

            //Title
            Text(
              'Verify Code',
              style: TextStyle(fontSize: 32),
            ),

            //TextField
            // TextFieldComponent(
            //   controller: _emailEditingController,
            //   labelText: 'Enter your email ID',
            //   validator: (String value) {
            //     if (RegExp(
            //             r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            //         .hasMatch(value)) return null;
            //     return 'Enter valid email';
            //   },
            // ),

            //Send reset email
            Builder(builder: (context) {
              return FlatButton(
                onPressed: () {
                  Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text('Reset email has been sent')),
                  );
                },
                child: Text(
                  'Send reset email',
                  style: TextStyle(
                    fontSize: 22,
                    color: Colors.white,
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                // minWidth: MediaQuery.of(context).size.width * .7,
                color: Theme.of(context).accentColor,
              );
            }),
            SizedBox(height: 16),

            GestureDetector(
              onTap: () => Navigator.pushReplacementNamed(context, 'login'),
              child: RichText(
                text: TextSpan(
                  style: TextStyle(color: Colors.black),
                  children: [
                    TextSpan(text: 'Back to '),
                    TextSpan(
                      text: 'Login',
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
