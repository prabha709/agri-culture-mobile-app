import 'dart:io';
import 'package:agri/NotifiersModel/CartModel.dart';
import 'package:agri/screens/cart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:scoped_model/scoped_model.dart';

class ProductDetailScreen extends StatefulWidget {
  final indexData;
  final userDetails;

  ProductDetailScreen({
    this.indexData,
    this.userDetails
  });


  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
 _addToCart(indexProduct) async{
    String url = "https://api-agriculture.datavivservers.in/order/";
    try {
      Map data = {
        "user_id": widget.userDetails["userId"],
        "product_id": indexProduct.toString()
      };
      var response = await http.post(
        "$url",
        body: data,
        headers: {"Authorization":"Token ${widget.userDetails["token"]}"}
      );
      
     var jsonObject = json.decode(response.body);
        print(jsonObject);
      if (response.statusCode == 200) {
        
       return Flushbar(
          message:"Product Added to Cart",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } else {
        return Flushbar(
          message: "Product not Added to cart",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
    } catch (error) {}
  }
Future getCategoryProducts()async{

  String url ="https://api-agriculture.datavivservers.in/GetOneProductDetailsAPI/${widget.indexData["id"]}/";
  try {
        var response = await http.get(
          "$url",
        );
       

        if (response.statusCode == 200) {
           var jsonObject = json.decode(utf8.decode(response.bodyBytes));
          return jsonObject[0];
        } else {
          return Flushbar(
            message: "Something Went wrong Please try again later",

            duration: Duration(seconds: 3),
            flushbarPosition: FlushbarPosition.BOTTOM,
            // flushbarStyle: FlushbarStyle.FLOATING,
          )..show(context);
        }
      } on SocketException catch (error) {
        return Flushbar(
          message: "${error.message}",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } catch (error) {
        return Flushbar(
          message: "${error.message}",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }

}




  String dropdownValue = '1 kg';


  @override
  Widget build(BuildContext context) {
    CartModel model = CartModel(userDetails: widget.userDetails);
    return ScopedModel(
       model: model,

          child: Scaffold(
          appBar: AppBar(
          elevation: 0,
          
          title: Text(
      widget.indexData["product_name"],
      // style: TextStyle(fontSize: 14),
          ),
          actions: [
       ScopedModelDescendant<CartModel>(
         builder: (BuildContext context,Widget child,CartModel cartModel){
          return FutureBuilder(
           future: cartModel.getCount(),
           builder:(context, snapshot){
             if(snapshot.data == null){
               return Padding(
                 padding: const EdgeInsets.all(15.0),
                 child: Icon(Icons.shopping_cart),
               );
             }else{
               return Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: new IconButton(
                onPressed: () {
                  Navigator.pop(context);
                    Navigator.push(
          context, MaterialPageRoute(builder: (context) =>CartScreen(userToken: widget.userDetails,) ));
                },
                icon: Icon(Icons.shopping_cart),
              ),
            ),
            new Icon(
                          Icons.brightness_1,
                          size: 21.0, color: Colors.red[800]),
                          new Positioned(
                          top: 3.0,
                          right:44,
                          child: new Center(
                            child: new Text(snapshot.data["get_cart_items"].toString(),
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 11.0,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          )),
          ],
        );
             }
           } );

        },
                   
      )

           
          ],
          
        ),
          body: Padding(
      padding: const EdgeInsets.all(8.0),
      child: FutureBuilder(
        future:getCategoryProducts(),
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.done){
            if(snapshot.data == null){
                 return Center(
         child:Text("No Products found"),
       );
            }else{
              return Column(
        children: [
          //Product Name
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              alignment: Alignment.centerLeft,
              child: Text(snapshot.data["product_name"],
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),

          //Product Image
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 32,
            ),
            child: Container(
              height: MediaQuery.of(context).size.height/3,
              child: Image(
              
                
                 image:snapshot.data["product_images"] == ""?AssetImage("assets/Logo.png"): NetworkImage("https://api-agriculture.datavivservers.in"+snapshot.data["product_images"]),
              ),
            ),
          ),

          //Weight select
          // Container(
          //   margin: EdgeInsets.symmetric(vertical: 12, horizontal: 32),
          //   padding: EdgeInsets.symmetric(horizontal: 32),
          //   decoration: BoxDecoration(
          //       border: Border.all(color: Colors.grey),
          //       borderRadius: BorderRadius.circular(10)),
          //   child: DropdownButton<String>(
          //     value: dropdownValue,
          //     isExpanded: true,
          //     icon: Icon(Icons.arrow_drop_down),
          //     iconSize: 24,
          //     elevation: 16,
          //     style: TextStyle(color: Colors.grey),
          //     onChanged: (String newValue) {
          //       setState(() {
          //         dropdownValue = newValue;
          //       });
          //     },
          //     underline: Container(color: null),
          //     items: <String>['1 kg', '2 kg', '3 kg', '4 kg']
          //         .map<DropdownMenuItem<String>>((String value) {
          //       return DropdownMenuItem<String>(
          //         value: value,
          //         child: Text(value),
          //       );
          //     }).toList(),
          //   ),
          // ),

          //Original Price
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RichText(
              text: TextSpan(
                style: TextStyle(color: Colors.grey),
                children: [
                  TextSpan(text: 'MRP. ₹ '),
                  TextSpan(
                    text: snapshot.data["product_price_mrp"].toString(),
                    style: TextStyle(decoration: TextDecoration.lineThrough),
                  ),
                ],
              ),
            ),
          ),

          //Discount Price
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              '₹ ${snapshot.data["product_price"]}/1kg',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),

          //Add to Cart Button
          FlatButton(
            onPressed: () {
              
              _addToCart(snapshot.data["product_id"]);
              // addProductsTocart();
            },
            color: Theme.of(context).accentColor,
            padding: EdgeInsets.symmetric(horizontal: 64),
            child: Text(
              'ADD TO CART',
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          )
          //About this product
        ],
      );
            }
          }else if(snapshot.hasError){
            return Center(
             
       child: Text("Internal Server Error"),
           );
          }else{
            return Center(
          child:SpinKitDoubleBounce(
            size: 70.0,
        color: Theme.of(context).accentColor,
      ));
          }
        },
      ),
      
          ),
        ),
    );
  }
}
