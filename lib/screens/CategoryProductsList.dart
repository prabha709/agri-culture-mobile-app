import 'dart:io';
import 'package:agri/NotifiersModel/CartModel.dart';
import 'package:agri/screens/cart.dart';
import 'package:agri/screens/productdetail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';

import 'package:scoped_model/scoped_model.dart';

class CategoryProductsList extends StatefulWidget {
  final indexData;
  final categoryName;
  final userDetails;

  CategoryProductsList({this.indexData, this.categoryName, this.userDetails});

  @override
  _CategoryProductsListState createState() => _CategoryProductsListState();
}

class _CategoryProductsListState extends State<CategoryProductsList> {
  Future getCategoryList() async {
    String url =
        "https://api-agriculture.datavivservers.in/GetProductByCategory/${widget.indexData}/";
    try {
      var response = await http.get(
        "$url",
      );
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        
        return jsonObject;
      }else if(response.statusCode == 404){
        return null;
      
      } 
      
      else {
        print(response.statusCode);
       return Center(
          child:Column(
            children: <Widget>[
              Icon(Icons.warning,color: Colors.red,),
              Text("Internal Server Issue")
            ],
          ) ,
        );
      }
    } on SocketException catch (error) {
       Flushbar(
        message: "${error.message}",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      return null;
    } catch (error) {
      Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      return null;
    }
  }

  _addProductsToCart(indexProduct) async {
    String url = "https://api-agriculture.datavivservers.in/order/";
    try {
      Map data = {
        "user_id": widget.userDetails["userId"].toString(),
        "product_id": indexProduct.toString()
      };
      var response = await http.post("$url",
          body: data,
          headers: {"Authorization": "Token ${widget.userDetails["token"]}"});
     

      if (response.statusCode == 200) {
         var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        return Flushbar(
          message: "Product Added to Cart",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } else {
        return Flushbar(
          message: "Product not Added to cart",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
    } catch (error) {
      return Flushbar(
        message: error.message,

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    }
  }

  _navigateToProducts(indexData) {
    Navigator.pop(context);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductDetailScreen(
                  indexData: indexData,
                  userDetails: widget.userDetails,
                )));
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
     CartModel model = CartModel(userDetails: widget.userDetails);
    return ScopedModel(
      model:model,
child: Scaffold(
          appBar: AppBar(
      title: Text(widget.categoryName.toString()),
      actions: [
       ScopedModelDescendant<CartModel>(builder: (BuildContext context,Widget child,CartModel cartModel){
          return FutureBuilder(
           future: cartModel.getCount(),
           builder:(context, snapshot){
             if(snapshot.data == null){
               return Padding(
                 padding: const EdgeInsets.all(15.0),
                 child: Icon(Icons.shopping_cart),
               );
             }else{
               return Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: new IconButton(
                onPressed: () {
                  Navigator.pop(context);
                    Navigator.push(
          context, MaterialPageRoute(builder: (context) =>CartScreen(userToken: widget.userDetails,) ));
                },
                icon: Icon(Icons.shopping_cart),
              ),
            ),
            new Icon(
                          Icons.brightness_1,
                          size: 21.0, color: Colors.red[800]),
                          new Positioned(
                          top: 3.0,
                          right:44,
                          child: new Center(
                            child: new Text(snapshot.data["get_cart_items"].toString(),
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 11.0,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          )),
          ],
        );
             }
           } );

        },
                   
      )

           
          ],
          ),
          body: FutureBuilder(
      future: getCategoryList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Center(
              child: Text("No Products found"),
            );
          } else {
            return GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: itemWidth / itemHeight),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      _navigateToProducts(snapshot.data[index]);
                    },
                    child: Container(
                      // height: 260,
                      // width: 170,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Color(0xffc4c4c4),
                          )),
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.all(8),
                      child: Column(
                        children: [
                          //Product Image
                          Expanded(
                            child: Image(
                              fit: BoxFit.cover,
              

                                image:snapshot.data[index]["product_images"] == null?AssetImage("assets/Logo.png"):NetworkImage("https://api-agriculture.datavivservers.in${snapshot.data[index]["product_images"]}"),
                            ),
                          ),

                          //Product name
                          Text(
                            snapshot.data[index]["product_name"],
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),

                          //Product Price
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              //orignal price
                              Text(
                                '₹' +
                                    snapshot.data[index]["product_price_mrp"]
                                        .toString(),
                                style: TextStyle(
                                    decoration: TextDecoration.lineThrough),
                              ),
                              SizedBox(width: 8),
                              //offer price
                              Text(
                                '₹' +
                                    snapshot.data[index]["product_price"]
                                        .toString(),
                                style: TextStyle(
                                  // decoration: TextDecoration.lineThrough,
                                  color: Theme.of(context).primaryColor,
                                ),
                              )
                            ],
                          ),

                          //add To Cart button
                          FlatButton(
                            onPressed: () {
                             
                              _addProductsToCart(
                                  snapshot.data[index]["product_id"]);
                            },
                            color: Theme.of(context).accentColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Text(
                              'ADD TO CART',
                              style: TextStyle(color: Colors.white),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                });
          }
        } else if (snapshot.hasError) {
          return Center(
            child: Text("Internal Server Error"),
          );
        } else {
          return Center(
              child: SpinKitDoubleBounce(
            size: 70.0,
            color: Theme.of(context).accentColor,
          ));
        }
      },
          ),
        ),
    );
  }
}
