import 'dart:io';
import 'package:agri/screens/main.dart';
import 'package:agri/NotifiersModel/CartModel.dart';
import 'package:agri/screens/CategoryProductsList.dart';
import 'package:agri/screens/cart.dart';
import 'package:agri/screens/orders.dart';
import 'package:agri/screens/productdetail.dart';
import 'package:agri/screens/settings.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';




class HomeApp extends StatelessWidget {
  
  final userDetails;

  HomeApp({
    this.userDetails
  });
  @override
  Widget build(BuildContext context) {
    CartModel model = CartModel(userDetails: userDetails);
    return ScopedModel(
      model: model, 
      child:  MaterialApp( 
       theme: ThemeData(
        primaryColor: Color(0xff9DCF9C),
        accentColor: Color(0xff50A94E),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: HomeScreen(userDetials: userDetails,),
        
      ));
    
  }
}


class HomeScreen extends StatefulWidget {
  final userDetials;
  

  HomeScreen({
    this.userDetials,
    
    
    });

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

String searchString;


  _addToCart(indexProduct) async{
    String url = "https://api-agriculture.datavivservers.in/order/";
    try {
      Map data = {
        "user_id": widget.userDetials["userId"].toString(),
        "product_id": indexProduct.toString()
      };
      var response = await http.post(
        "$url",
        body: data,
        headers: {"Authorization":"Token ${widget.userDetials["token"]}"}
      );
     
     
      if (response.statusCode == 200) {
         var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        
       return Flushbar(
          message:"Product Added to Cart",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } else {
        return Flushbar(
          message: "Product not Added to cart",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
    } catch (error) {}
  }

  Future getProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
         var tokenData= prefs.getString('token');
         var token = json.decode(tokenData);
    String url = "https://api-agriculture.datavivservers.in/HomePageContentAPI/";
    try {
      var response = await http.get(
        "$url",
        headers: {"Authorization":"Token ${token["token"]}"}
        
      );
     
      if (response.statusCode == 200) {
         var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        return jsonObject;
      } else {
         Flushbar(
          message: "Something Went wrong Please try again later",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
        return null;
      }
    } on SocketException catch (error) {
       Flushbar(
        message: "${error.message}",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      return null;
    } catch (error) {
       Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      return null;
    }
  }

  _navigateToCategoryProductsList(indexData, categoryName) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CategoryProductsList(
                  indexData: indexData,
                  categoryName: categoryName,
                  userDetails: widget.userDetials,
                )));
  }

  _navigateToProducts(indexData) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductDetailScreen(
                  indexData: indexData,
                  userDetails: widget.userDetials,
                )));
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;

    return Scaffold(
      resizeToAvoidBottomInset: false,
    appBar: AppBar(
      elevation: 0,
      centerTitle: true,
      title: Text(
        'Farmers alliance for business',
        style: TextStyle(fontSize: 14),
      ),
      actions: [
   ScopedModelDescendant<CartModel>(builder: (BuildContext context,Widget child,CartModel cartModel){
return FutureBuilder(
       future: cartModel.getCount(),
       builder:(context, snapshot){
         
         if(snapshot.data == null){
           return Padding(
             padding: const EdgeInsets.all(15.0),
             child: Icon(Icons.shopping_cart),
           );
         }else{
           return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: new IconButton(
            onPressed: () {
                Navigator.push(
      context, MaterialPageRoute(builder: (context) =>CartScreen(userToken: widget.userDetials,) ));
            },
            icon: Icon(Icons.shopping_cart),
          ),
        ),
        new Icon(
                      Icons.brightness_1,
                      size: 21.0, color: Colors.red[800]),
                      new Positioned(
                      top: 3.0,
                      right:44,
                      child: new Center(
                        child: new Text(snapshot.data["get_cart_items"].toString(),
                          style: new TextStyle(
                              color: Colors.white,
                              fontSize: 11.0,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      )),
      ],
    );
         }
         
       } );
        
      
      

    },
               
        )

       
      ],
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: Container(
    color: Theme.of(context).primaryColor,
    padding: EdgeInsets.only(bottom: 8, left: 16, right: 16),
    child: Container(
      padding: EdgeInsets.only(left: 8),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: TextFormField(
        onChanged: (value){
          setState(() {
            searchString = value;
          });
        },
        onTap: () {
         return showSearch(context: context, delegate: DataSearch(searchString: searchString));
        },
        // onChanged: (String text){
        //
        // },
        style: TextStyle(fontSize: 18),
        cursorColor: Colors.black,
        decoration: InputDecoration(
          border: InputBorder.none,
          prefixIcon: Icon(
            Icons.search,
            color: Colors.grey,
          ),
          hintText: 'Search',
        ),
      ),
    ),
        ),
      ),
    ),
    drawer: Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
    DrawerHeader(
      decoration: BoxDecoration(
        color: Color(0xff9DCF9C),
      ),
      child: Container(
        child: Center(
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 32,
                  child: widget.userDetials["user_images_URL"] == "" ?
                  Center(child: Icon(Icons.camera)):
                  Image.network("https://api-agriculture.datavivservers.in"+widget.userDetials["user_images_URL"])
                  
                ),
              ),
              widget.userDetials["token"] == null
                  ? Text('Welcome User')
                  : Text(widget.userDetials["first_name"]==null?"Guest":widget.userDetials["first_name"]),
            ],
          ),
        ),
      ),
    ),
    //Home
    ListTile(
      leading: Icon(Icons.home),
      title: Text('Home'),
    onTap: (){
      Navigator.pop(context);
    },
    ),
    //Category
    ListTile(
      leading: Icon(Icons.category),
      title: Text('Category'),
      onTap: () {},
    ),
    //Your Orders
    ListTile(
      leading: Icon(Icons.shopping_basket),
      title: Text('Your Orders'),
      onTap: () {
        Navigator.pop(context);
                    Navigator.push(
    context, MaterialPageRoute(builder: (context) =>OrdersScreen(userDetails: widget.userDetials,) ));
    

      },
    ),
    //FAQ's
    ListTile(
      leading: Icon(Icons.question_answer),
      title: Text('FAQs'),
      onTap: () {},
    ),
    //Contact
    ListTile(
      leading: Icon(Icons.contact_phone),
      title: Text('Contact'),
      onTap: () {},
    ),
    //Settings
    ListTile(
      leading: Icon(Icons.settings_applications),
      title: Text('Settings'),
      onTap: (){
        Navigator.pop(context);
         Navigator.push(
    context, MaterialPageRoute(builder: (context) =>SettingsScreen(userDetails: widget.userDetials) ));

      }
    ),
        ],
      ),
    ),
        body: FutureBuilder(
        future: getProducts(),
        builder: (context, snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.data == null) {
        return Center(
          child: Text("No Products found"),
        );
      } else {
        return Container(
          height: MediaQuery.of(context).size.height / 1.2,
          child: Column(
            children: <Widget>[
              Container(
                
                height: MediaQuery.of(context).size.height / 5,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: snapshot.data["product_category"].length,
                  itemBuilder: (_, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onTap: () {
                          _navigateToCategoryProductsList(
                              snapshot.data["product_category"][index]
                                  ["id"],
                              snapshot.data["product_category"][index]
                                  ["category_name"]);
                        },
                        child: Container(
                          child: Column(
                            children: [
                              CircleAvatar(
                                backgroundColor:
                                    Theme.of(context).primaryColor,
                                radius: 38,
                              backgroundImage:snapshot.data["product_category"][index]["product_thumbnail"]!=""? NetworkImage("https://api-agriculture.datavivservers.in"+snapshot.data["product_category"][index]["product_thumbnail"])
                              :AssetImage("assets/Logo.png")
                              ,
                            //  backgroundImage: NetworkImage("https://www.greenbiz.com/sites/default/files/images/articles/featured/seedsnopparatpromthasstock.png"),
                              ),
                              SizedBox(height: 8),
                              Text(snapshot.data["product_category"]
                                  [index]["category_name"]),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Top Products',
                  style: TextStyle(
                      fontSize: 24, fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                child: GridView.builder(
                    gridDelegate:
                        SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: itemWidth / itemHeight),
                    itemCount: snapshot.data["trending_products"].length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          _navigateToProducts(
                              snapshot.data["trending_products"][index]);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            
                              borderRadius: BorderRadius.circular(10),
                              
                              border: Border.all(
                                color: Color(0xffc4c4c4),
                                
                              )),
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.all(8),
                          child: Column(
                            children: [
                              //Product Image
                              Container(
                                height: MediaQuery.of(context).size.height/3.7,
                                child:Image(
                                  // image: NetworkImage("https://www.greenbiz.com/sites/default/files/images/articles/featured/seedsnopparatpromthasstock.png"),
                                  fit: BoxFit.cover,
                                 image: snapshot.data["trending_products"][index]["product_images_URL"] == ""? AssetImage("assets/Logo.png"):
                                 NetworkImage("https://api-agriculture.datavivservers.in"+snapshot.data["trending_products"][index]["product_images_URL"] )
                                
                                ),
                              ),

                              //Product name
                              Expanded(
                                                              child: Text(
                                  snapshot.data["trending_products"][index]
                                      ["product_name"],
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 13,
                                  ),
                                ),
                              ),

                              //Product Price
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.center,
                                children: [
                                  //orignal price
                                  Text(
                                    '₹' +
                                        snapshot.data["trending_products"]
                                                [index]["product_price_mrp"]
                                            .toString(),
                                    style: TextStyle(
                                        decoration:
                                            TextDecoration.lineThrough),
                                  ),
                                  SizedBox(width: 8),
                                  //offer price
                                  Text(
                                    '₹' +
                                        snapshot.data["trending_products"]
                                                [index]["product_price"]
                                            .toString(),
                                    style: TextStyle(
                                      // decoration: TextDecoration.lineThrough,
                                      color:
                                          Theme.of(context).primaryColor,
                                    ),
                                  )
                                ],
                              ),

                              //add To Cart button
                              FlatButton(
                                
                                onPressed: () {
                                 
                                  _addToCart(
                                    
                                      snapshot.data["trending_products"]
                                          [index]["product_id"]);
                                },
                                splashColor: Colors.redAccent,
                                color: Theme.of(context).accentColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),

                                ),
                                child: Text(
                                  'ADD TO CART',
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        );
      }
    } else if (snapshot.hasError) {
      return Center(
        child: Text("Internal Server Error"),
      );
    } else {
      return Center(
          child: SpinKitDoubleBounce(
        size: 70.0,
        color: Theme.of(context).accentColor,
      ));
    }
        }),
        );
  }
}
