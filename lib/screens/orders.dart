import 'dart:io';

import 'package:agri/screens/OrderDetails.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart'as http;
class OrdersScreen extends StatefulWidget {
final userDetails;

OrdersScreen({
  this.userDetails
});

  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {


  Future getOrders() async {
    String url = "https://api-agriculture.datavivservers.in/order/";
    try {
      var response = await http.get(
        "$url",
        headers: {"Authorization":"Token ${widget.userDetails["token"]}"}
      );
     
      if (response.statusCode == 200) {
          var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        if(jsonObject["code"]==200){
         print(jsonObject);
        return jsonObject["data"];
      }else{
        Flushbar(
        message: jsonObject["message"],

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      return null;

      }
         
        
      }
      
      else {
        return null;
      }
    } on SocketException catch (error) {
      return Flushbar(
        message: "${error.message}",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    } catch (error) {
      return Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    }
  }
  _navigateToOrderDetails(indexData){
    //      Navigator.push(
    // context, MaterialPageRoute(builder: (context) =>OrderDetailsPage(indexData: indexData,userDetails: widget.userDetails,) ));


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Orders"),
        
      ),
      body: FutureBuilder(
        future:getOrders() ,
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.done ){
            if(snapshot.data == null){
              return Center(
                child: Text("No Order Done Yet"),

              );
            }else{
              return ListView.builder(
                shrinkWrap: true,
                itemCount: snapshot.data.length,
                itemBuilder: (context, index){
                  // print(snapshot.data[index]["orderDetails"]);
                  return InkWell(
                   onTap: (){
                     _navigateToOrderDetails(snapshot.data[index]["id"]);
                   },
                   child: Card(
                     elevation: 5.0,
                     child: Container(
                  
                       height: MediaQuery.of(context).size.height/6.3,
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Container(
                               height: 20,
                               width: 100,
                               color: Colors.green,
                               child: Center(
                                 child: Text(snapshot.data[index]["order_status"],style: TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,color: Colors.white),)),
                             ),
                           Padding(
                             padding: const EdgeInsets.only(top:8.0,left: 8.0),
                             child: Text("Order Id:"+snapshot.data[index]["order_id"],style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.w800,color: Colors.grey),),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(left:8.0),
                             child: Text("Nov 30,2020; 10:23AM",style: TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,),),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top:8.0),
                             child: Divider(
                               thickness: 2.0,
                             ),
                           ),
                           Row(
                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                             children: [
                             Text("Payable Amount: Rs."+snapshot.data[index]["grandTotal"].toString(),style: TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,),),
                             Padding(
                               padding: const EdgeInsets.only(right:8.0),
                               child: Text("COD",style: TextStyle(fontSize: 15.0,fontWeight: FontWeight.bold,)),
                             )

                           ],)
                         ],
                       ),
                     ),
                   ),
                    //                   child: Card(
                    //   child: ListTile(
                    //     leading:Image.asset("assets/Logo.png"),
                    //     title: Text(snapshot.data[index]["order_id"]),
                    //     subtitle: Text("Total no of products\t"+snapshot.data[index]["get_cart_items"].toString()),
                    //     trailing: Padding(
                    //       padding: const EdgeInsets.only(right:18.0),
                    //       child: Text("Rs\t"+snapshot.data[index]["get_cart_sub_total"].toString()),
                    //     ),
                    //   ),
                      
                    // ),
                  );

                });
            }
          }else if(snapshot.hasError){
            return Center(child: Text("Internal Server Error"),);
          }else{
            return Center(
              child:SpinKitDoubleBounce(
                color: Theme.of(context).primaryColor,
              ) ,
            );
          }

        },
        )
      
    );
  }
}
