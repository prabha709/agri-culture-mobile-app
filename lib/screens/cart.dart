import 'dart:io';
import 'package:agri/screens/address.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class CartScreen extends StatefulWidget {
  final userToken;

  CartScreen({this.userToken});

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  String operationAction;

  getCartDetails() async {
    String url = "https://api-agriculture.datavivservers.in/CartDetails/";
    try {
      var response = await http.get('$url',
          headers: {"Authorization": "Token ${widget.userToken["token"]}"});
      
      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        return jsonObject;
      } 
      else {
        return Flushbar(
          message: "Something Went wrong Please try again later",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
    } on SocketException catch (error) {
      return Flushbar(
        message: error.message,

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    } catch (error) {
      print(error);
    }
  }

  _incrementProduct(productId) async {
    String url = "https://api-agriculture.datavivservers.in/OrderActionDetail/";
    try {
      Map data = {
        "user_id": widget.userToken["userId"].toString(),
        "product_id": productId,
        "action": operationAction
      };
      var response = await http.post('$url',
          body: data,
          headers: {"Authorization": "Token ${widget.userToken["token"]}"});
     
      if (response.statusCode == 200) {
         var jsonObject = json.decode(response.body);
        return Flushbar(
        message: "$operationAction is done",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
        
      } else {
        return Flushbar(
        message: "$operationAction is not done",
backgroundColor: Colors.red,        
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      }
    } catch (error) {
      return Flushbar(
        message: error.message,

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);

    }
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        appBar: AppBar(
    title: Text("Shopping Bag"),
        ),
        body:   FutureBuilder(
      future: getCartDetails(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data["products"].length == 0) {
            return Center(
              child: Text("No Product Added"),
            );
          } else {
            return Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 18.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 28.0),
                            child: Text("Item",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left:18.0),
                            child: Text("quantity",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Padding(
                              padding: EdgeInsets.only(right: 38.0),
                              child: Text("Price",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  )))
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 2.0,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height/2,
                     
                      child: ListView.separated(
                        shrinkWrap: true,
                          separatorBuilder:
                              (BuildContext context, int index) => Divider(
                                    thickness: 2.0,
                                  ),
                          itemCount: snapshot.data["products"].length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(top: 18.0),
                              child: ListTile(
                                leading: CircleAvatar(
                                  radius: 18,
                                  backgroundImage: 
                                   NetworkImage("https://api-agriculture.datavivservers.in"+snapshot.data["products"][index]["product_images"])
                                  // NetworkImage("https://www.greenbiz.com/sites/default/files/images/articles/featured/seedsnopparatpromthasstock.png"),
                                ),
                                trailing: Padding(
                                        padding: const EdgeInsets.only(right: 25.0),
                                        child: Text("Rs.\t" +snapshot.data["products"][index]["get_product_total"].toString()),),
                                title: Row(
                              children: <Widget>[
                                      Container(
                                       width: MediaQuery.of(context).size.width/4.7,
                                        child: Text(snapshot.data["products"]
                                            [index]["product_name"],overflow: TextOverflow.ellipsis,
                                            style: TextStyle(fontSize: 13.0),
                                            ),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width/3.2,
                                       child: Row(
                                          children: <Widget>[
                                            new IconButton(
                                              icon: Icon(Icons.add,size: 25,), onPressed: 
                                              (){
                                                setState(() {
                                                  operationAction ="increment";
                                               });
                                              _incrementProduct(
                                              snapshot.data["products"][index]["product_id"]);
                                              }
                                              ),
                                            Text(snapshot.data["products"][index]["product_quantity"].toString()),
                                            new IconButton(
                                              icon: Icon(Icons.remove,size: 30,), onPressed: 
                                              (){
                                                 setState(() {
                                                   operationAction = "decrement";
                                                 });
                                                 _incrementProduct(
                                                 snapshot.data["products"][index]["product_id"]);
                                              }
                                              )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                              ),
                            );
                          }),
                    ),
                    
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 10.0, left: 18.0, right: 18.0),
                      child: Container(
                        height: MediaQuery.of(context).size.height / 5,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(18.0)),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 18.0, right: 18.0, top: 15.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Sub Total",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Rs.\t" +snapshot.data["get_cart_sub_total"].toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 18.0, right: 18.0, top: 15.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Total Savings",style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text("Rs.\t" +snapshot.data["get_cart_total_discount"].toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 18.0, right: 18.0, top: 15.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Delivery Charges",style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Rs.\t" +snapshot.data["deliveryCharges"].toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            Divider(
                              thickness: 2.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 18.0, right: 18.0, top: 15.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Total Amount".toUpperCase(),
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Rs.\t" +snapshot.data["get_cart_total"].toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              ),
                            
                          ],
                        ),
                      ),
                    ),
                    Padding(
                              padding: EdgeInsets.only(bottom: 0.0),
                              child: FlatButton(
                               onPressed: () {
                                        Navigator.pop(context);
                                           Navigator.push(
          context, MaterialPageRoute(builder: (context) =>AddressScreen(
            userToken: widget.userToken,totalAmount: snapshot.data["get_cart_total"],
            
            ) ));
},
                          color: Theme.of(context).accentColor,
                          shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Text(
                                        'Check Out'.toUpperCase(),
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                          
                            )
                  ],
                ));
          }
        } else if (snapshot.hasError) {
          return Center(
            child: Text("Internal Server Error"),
          );
        } else {
          return Center(
              child: SpinKitDoubleBounce(
            size: 70.0,
            color: Theme.of(context).accentColor,
          ));
        }
      },
    )

        
            
        
      );
  }
}
