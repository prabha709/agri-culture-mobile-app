import 'package:agri/screens/addaddress.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
class AddressScreen extends StatefulWidget {
  final userToken;
  final totalAmount;

  AddressScreen({
    this.totalAmount,
    this.userToken});
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
Razorpay _razorpay;


  bool loading = false;
  getAddressDetails() async {
    String url = "https://api-agriculture.datavivservers.in/ShippingAddress/";

    try {
      var response = await http.get("$url",
          headers: {"Authorization": "Token ${widget.userToken["token"]}"});

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        return jsonObject[0];
      } else {
        Center(
          child: Text("Address not Added yet"),
        );
        Flushbar(
          message: "Add your Current Addresss",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
    } on SocketException catch (error) {
      return Flushbar(
        message: "${error.message}",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    } catch (error) {
      return Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    }
  }

  _orderProduct() async {
    String url = "https://api-agriculture.datavivservers.in/placeOrderViewForWeb/";
   
    try {
      var response = await http.post("$url",
          headers: {"Authorization": "token ${widget.userToken["token"]}"});
      var jsonObject = json.decode(response.body);
      print(jsonObject);
      if (response.statusCode == 200) {
        Navigator.pop(context);
         return Flushbar(
        message: "Order Done Sucessfully",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);

     
      } else {
       return Flushbar(
        message: "Order Not done Properly",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
      }
    } on SocketException catch (error) {
      return Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    } catch (error) {
      return Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    }
  }
@override
void initState(){

  super.initState();
  _razorpay = Razorpay();
  _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSucess);
   _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
}
@override
void dispose(){
  super.dispose();
  _razorpay.clear();
}
void openCheckout()async{
  var options = {
    "key":"rzp_test_ieQTyJtg8NpSaf",
    "amount":widget.totalAmount * 100,
    "name":"FAB",
    "description":"Test Payment",
    "prefill":{"contact":"","email":""},
    "external":{
      "wallets":["paytm"]
    }

  };
  try{
    _razorpay.open(options);

  }catch(e){
    debugPrint(e);

  }
}
void _handlePaymentSucess(PaymentSuccessResponse response){
   Fluttertoast.showToast(msg: "SUCCESS"+response.paymentId + "Please wait until the Order Placed");
 return _orderProduct();
 
}
void _handlePaymentError(PaymentFailureResponse response){
  Fluttertoast.showToast(msg: "ERROR"+ response.code.toString()+ "\t" +response.message);
}
void _handleExternalWallet(ExternalWalletResponse response){
  Fluttertoast.showToast(msg: "External Wallet".toUpperCase()+response.walletName);

}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Addresses'),
      ),
      body: Column(
        children: [
          //Add address button
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AddAddressScreen(
                            userToken: widget.userToken,
                          )));
            },
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 16, horizontal: 8.0),
              child: Text(
                '+ Add a new Address',
                style: TextStyle(
                  fontSize: 18,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
          Divider(color: Colors.grey, thickness: 2),
          SizedBox(height: 8),

          Container(
            padding: EdgeInsets.all(8),
            color: Colors.grey[200],
            child: Center(
              child: Text('Saved Addresses'),
            ),
          ),

          //All Addresses
          FutureBuilder(
              future: getAddressDetails(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.data == null) {
                    return Center(
                      child: Text(
                        "Address not yet Added",
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                    );
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(left: 18.0),
                      child: ListView(
                        shrinkWrap: true,
                        children: [
                          Card(
                            child: ListTile(
                              leading: Icon(Icons.radio_button_checked,color: Colors.green),
                              title: Text("prabhakaran"),
                              subtitle: Row(
                            children: [
                              Text(snapshot.data["address1"]),
                              SizedBox(width: 2,),
                          Text(snapshot.data["address2"]),
                          SizedBox(width: 2,),
                          Text(snapshot.data["pincode"]),
                         
                            ],
                          ), 


                            ),
                          ),
                          
                         
                          
                          Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: Center(
                              child: FlatButton(
                                onPressed: () {
                                  _orderProduct();
                                //  openCheckout();
                                },
                                color: Theme.of(context).accentColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Text(
                                  'Order On COD'.toUpperCase(),
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text("Something Went Wrong Internal Server Error"),
                  );
                } else {
                  return Center(
                      child: SpinKitDoubleBounce(
                    size: 70.0,
                    color: Theme.of(context).accentColor,
                  ));
                }
              })
        ],
      ),
    );
  }
}
