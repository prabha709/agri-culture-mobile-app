import 'dart:io';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:agri/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MainScreen extends StatefulWidget{
  final userToken;

  MainScreen({this.userToken});
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
            body: HomeApp(
        userDetails: widget.userToken,
        
        
      ),
    );
  }
}

getProductsList() async {
  String url = "https://api-agriculture.datavivservers.in/ProductSearchAPI/";
  try {
    var response = await http.get(
      "$url",
    );
    
    if (response.statusCode == 200) {
      var jsonObject = json.decode(utf8.decode(response.bodyBytes));
      return jsonObject;
    } else {
      print("something Went wrong");
    }
  } on SocketException catch (error) {
    print(error);
  } catch (error) {
    print(error);
  }
}

class DataSearch extends SearchDelegate<String>{

  static String searchString;

  DataSearch({
    searchString
  });

final data = searchString;
  final products = FutureBuilder(
      future: getProductsList(),
      // ignore: top_level_function_literal_block
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Center(
              child: Text("No Products found"),
            );
          } else {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Icon(Icons.phone_android),
                    title: Text(snapshot.data[index]["product_name"]),
                  );
                });
          }
        } else if (snapshot.hasError) {
          return Center(
            child: Text('something went wrong'),
          );
        } else {
          return Center(
            child: SpinKitDualRing(
              color: Theme.of(context).primaryColor,
            ),
          );
        }
      });

  @override
  List<Widget> buildActions(context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  Widget buildLeading(context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.arrow_menu,
        progress: transitionAnimation,
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Widget buildSuggestions(context) {
    return products ;
  }

  Widget buildResults(context) {
    
  }
}
