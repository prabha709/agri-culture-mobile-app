import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
class OrderDetailsPage extends StatelessWidget {
 final indexData;
 final userDetails;

 OrderDetailsPage({
   this.indexData,
   this.userDetails
 });

  @override
  Widget build(BuildContext context) {



  Future getOrdersDetails() async {
    String url = "https://fab-agri-api.herokuapp.com/order/$indexData";
    try {
      var response = await http.get(
        "$url",
        headers: {"Authorization":"Token ${userDetails["token"]}"}
      );
     
      if (response.statusCode == 200) {
         var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        return jsonObject;
      } else {
        return null;
      }
    } on SocketException catch (error) {
      return Flushbar(
        message: "${error.message}",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    } catch (error) {
      return Flushbar(
        message: "${error.message}",
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    }
  }
    return Scaffold(
      appBar: AppBar(
        title: Text("Order Again"),
      ),
      body: FutureBuilder(
        future: getOrdersDetails(),
        builder: (context, snapshot){
          if(snapshot.connectionState ==  ConnectionState.done){
            if(snapshot.data== null){
              return Center(
                child: Text("No Products Available Here"),

              );
            }else{
              return ListView.builder(
                itemCount: snapshot.data["products"].length,
                itemBuilder: (context, index){
                  return Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Card(
                      elevation: 2.0,
                                          child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
                        leading: Image.network(snapshot.data["products"][index]["product_images"]),
                        title: Text(snapshot.data["products"][index]['product_name']),
                        trailing: Padding(
                          padding: const EdgeInsets.only(right:18.0),
                          child: Text("Rs.\t"+snapshot.data["products"][index]["product_price"].toString()),
                        ),
                      ),
                    ),
                  );
                });
            }
          }else if(snapshot.hasError){
            return Center(
              child: Text("Internal Server Error"),
            );
          }else{
            return Center(
              child: SpinKitDoubleBounce(
                color: Theme.of(context).accentColor,
              ),
            );
          }
        }),
      
    );
  }
}