import 'package:agri/Login_parts/login.dart';
import 'package:agri/screens/address.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatefulWidget {

final userDetails;

SettingsScreen({
  this.userDetails
});

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {


  _logOut()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("token");
    Navigator.pushAndRemoveUntil(
            context, MaterialPageRoute(builder: (context) => LoginScreen()),(Route<dynamic> route) => false);
    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Settings'),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Icon(
              Icons.shopping_cart,
            ),
          )
        ],
      ),

      //Body
      body: Column(
        children: [
          //header
          Container(
            color: Theme.of(context).primaryColor,
            child: Padding(
              padding: const EdgeInsets.only(top: 32, bottom: 8),
              child: Center(
                child: Column(
                  children: [
                    //Avatar
                    CircleAvatar(
                      radius: 40,
                    backgroundImage:widget.userDetails["user_images_URL"] == "" ?
                    AssetImage("assets/Logo.png"):
                    
                    NetworkImage(widget.userDetails["user_images_URL"]) ,
                    ),
                    SizedBox(height: 8),

                    //Usenrame
                    Text(widget.userDetails["first_name"],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 16),

                    // Phone No.
                    Text(widget.userDetails["mob_no"].toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                    ),

                    // Email
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(widget.userDetails["email"],
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                        GestureDetector(
                          onTap: () =>
                              Navigator.of(context).pushNamed('editprofile'),
                          child: Icon(Icons.edit),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),

          //Rest of the body
          //My orders
          Expanded(
            child: Container(
              color: Colors.grey[200],
              child: Column(
                children: [
                  //My Orders
                        Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("My Orders",
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(height: 8),
          Divider(
            color: Colors.grey,
          ),
          GestureDetector(
            // onTap: () => Navigator.pushNamed(context, navigatorScreenName),
            child: Container(
              alignment: Alignment.centerRight,
              child: Text("View All Orders".toUpperCase(),
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
        ],
      ),
    ),

                  //My wallet
                        Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("My Wallet",
                style: TextStyle(fontSize: 18),
              ),
              Text("Rs\t\t" + widget.userDetails["wallet_amount"].toString())
            ],
          ),
          SizedBox(height: 8),
          Divider(
            color: Colors.grey,
          ),
          GestureDetector(
            // onTap: () => Navigator.pushNamed(context, navigatorScreenName),
            child: Container(
              alignment: Alignment.centerRight,
              child: Text("View Details".toUpperCase(),
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
        ],
      ),
    ),

                  //My addresses
                             Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("My Addresses",
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(height: 8),
          Divider(
            color: Colors.grey,
          ),
          GestureDetector(
            // onTap: () => Navigator.pushNamed(context, navigatorScreenName),
            child: Container(
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: (){
                   Navigator.push(
            context, MaterialPageRoute(builder: (context) =>AddressScreen(userToken: widget.userDetails,) ));


                },
                              child: Text("View All".toUpperCase(),
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),

                  //Notification
                            Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Notification",
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(height: 8),
          Divider(
            color: Colors.grey,
          ),
          GestureDetector(
            // onTap: () => Navigator.pushNamed(context, navigatorScreenName),
            child: Container(
              alignment: Alignment.centerRight,
              child: Text("View All".toUpperCase(),
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
        ],
      ),
    ),

                  //Logout
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(
                        vertical: 12.0, horizontal: 4),
                    margin: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: InkWell(
                      onTap: (){
                        _logOut();
                      },
                                          child: Text(
                        'Logout',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
