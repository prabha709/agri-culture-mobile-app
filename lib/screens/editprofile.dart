
import 'package:flutter/material.dart';

class EditProfileScreen extends StatelessWidget {
  //Controllers
  final TextEditingController _firstnameEditingController =
      TextEditingController();
  final TextEditingController _lastnameEditingController =
      TextEditingController();
  final TextEditingController _phonenoEditingController =
      TextEditingController();
  final TextEditingController _emailEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //Form
            Form(
              child: Column(
                children: [
                  // TextFieldComponent(
                  //   controller: _firstnameEditingController,
                  //   labelText: 'First Name',
                  // ),
                  // TextFieldComponent(
                  //   controller: _lastnameEditingController,
                  //   labelText: 'Last Name',
                  // ),
                  // TextFieldComponent(
                  //   controller: _phonenoEditingController,
                  //   labelText: 'Mobile Number',
                  // ),
                  // TextFieldComponent(
                  //   controller: _emailEditingController,
                  //   labelText: 'Email ID',
                  // ),
                ],
              ),
            ),

            //Save Button
            FlatButton(
              onPressed: () {},
              padding: EdgeInsets.symmetric(horizontal: 64, vertical: 16),
              color: Theme.of(context).accentColor,
              child: Text(
                'SAVE',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
