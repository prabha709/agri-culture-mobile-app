// import 'package:agri/screens/CategoryProductsList.dart';
// import 'package:agri/screens/addaddress.dart';
// import 'package:agri/screens/address.dart';
// import 'package:agri/screens/cart.dart';
// import 'package:agri/screens/editprofile.dart';
// import 'package:agri/screens/forgotpassword.dart';
// import 'package:agri/Login_parts/login.dart';
// import 'package:agri/screens/main.dart';
// import 'package:agri/screens/orders.dart';
// import 'package:agri/screens/otp.dart';
// import 'package:agri/screens/productdetail.dart';
// import 'package:agri/screens/settings.dart';
// import 'package:flutter/material.dart';

// class RouteGenerator {
//   static Route<dynamic> generateRoute(RouteSettings settings) {
//     print(settings);
//     switch (settings.name) {
    
//       case 'main':
//         return MaterialPageRoute(builder: (_) => MainScreen());
//       case 'login':
//         return MaterialPageRoute(builder: (_) => LoginScreen());
//       case 'settings':
//         return MaterialPageRoute(builder: (_) => SettingsScreen());
//       case 'forgotpassword':
//         return MaterialPageRoute(builder: (_) => ForgotPasswordScreen());
//       case 'address':
//         return MaterialPageRoute(builder: (_) => AddressScreen());
//       case 'addaddress':
//         return MaterialPageRoute(builder: (_) => AddAddressScreen());
//       case 'productdetail':
//         return MaterialPageRoute(builder: (_) => ProductDetailScreen());
//       case 'cart':
//         return MaterialPageRoute(builder: (_) => CartScreen());
//       case 'orders':
//         return MaterialPageRoute(builder: (_) => OrdersScreen());
//       case 'editprofile':
//         return MaterialPageRoute(builder: (_) => EditProfileScreen());
//       case 'categoryList':
//         return MaterialPageRoute(builder: (_) => CategoryProductsList());
//       case 'otp':
//         Map arg = settings.arguments as Map;
//         return MaterialPageRoute(
//             builder: (_) => OtpCheckScreen(
//                   number: arg['phoneno'],
//                   password: arg['password'],
//                 ));
//       // case 'likescommentsscreen':
//       //   String arg = settings.arguments as String;
//       //   return MaterialPageRoute(
//       //       builder: (_) => LikesCommentsScreen(postid: arg));

//       // //Chat Screen
//       // case 'chatscreen':
//       //   Map arg = settings.arguments as Map;
//       //   return MaterialPageRoute(
//       //     builder: (_) => ChatScreen(
//       //       //Sendername
//       //       sendername: arg['sendername'],
//       //       //The id the user is sending message to
//       //       messengerId: arg['messengerId'],
//       //     ),
//       //   );
//       default:
//         return MaterialPageRoute(
//           builder: (_) => Scaffold(
//             body: Container(
//               child: Center(
//                 child: Text('Check Route Name'),
//               ),
//             ),
//           ),
//         );
//     }
//   }
// }
