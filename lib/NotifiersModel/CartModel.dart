import 'package:http/http.dart'as http;
import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';


class CartModel extends Model{

final userDetails;

CartModel({
  this.userDetails
});


getCount()async{
  

    String url ="https://api-agriculture.datavivservers.in/CartDetails/";
      var response = await http.get(
        '$url',
        headers: {"Authorization":"Token ${userDetails["token"]}"}
      );
       var jsonObject = json.decode(response.body);
       
       
if(response.statusCode==200){

 notifyListeners();
 
  return jsonObject;
}
else{
   
  return null;
}

}
}