import 'package:flushbar/flushbar.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart'as http;
import "dart:convert";

class ProductIncrementModel extends Model{

  final userToken;
  

  ProductIncrementModel({
    this.userToken,
      });

getCartDetails() async {
    String url = "https://fab-agri-api.herokuapp.com/CartDetails/";
    
      var response = await http.get('$url',
          headers: {"Authorization": "Token ${userToken["token"]}"});
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        
        notifyListeners();
        return jsonObject;
      } 
      else {
        
        return Flushbar(
        message: "Internal Server Error",

        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        // flushbarStyle: FlushbarStyle.FLOATING,
      );
      }
    
  }
}