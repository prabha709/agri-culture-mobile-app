import 'package:sms_autofill/sms_autofill.dart';
import 'dart:io';
import 'package:flutter_otp/flutter_otp.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:agri/screens/main.dart';
import 'package:agri/utils/raised_button.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginSCreenctate createState() => _LoginSCreenctate();
}

class _LoginSCreenctate extends State<LoginScreen> {
  FlutterOtp otp = FlutterOtp();
    String _code;
  String signature = "{{ app signature }}";
  String result;
  int enteredOtp;
  String phoneNumber;
  final myController = TextEditingController();
  bool otpDetails = false;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  bool loading = false;
  bool passWordVisible = true;
  void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }

  Future<void> _logIn() async {
    String url =
        "https://api-agriculture.datavivservers.in/register_user_for_same_un_and_pass/";
        String url2="https://api-agriculture.datavivservers.in/api-token-auth/";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();

      try {
        Map data = {
          "mobile_no": phoneNumber,
          "language":"hindi"
        };
        var response = await http.post(
          "$url",
          body: data,
        );
        print(response.body);

        if (response.statusCode == 200) {

              Map data = {
          "username": phoneNumber,
          "password":phoneNumber,
        };
        var response = await http.post("$url2",body: data);
        if(response.statusCode==200){
           setState(() {
            loading = false;
          });
          var jsonObject = json.decode(response.body);
          var userDetails = jsonObject;

          var encodeData = json.encode(userDetails);
            SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('token', encodeData);
          Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MainScreen(userToken: userDetails)));
        }

         } else if (response.statusCode == 401) {
          setState(() {
            loading = false;
          });
          return Flushbar(
            message: "Unable to login with the provided credentials",
            duration: Duration(seconds: 3),
            flushbarPosition: FlushbarPosition.TOP,
            // flushbarStyle: FlushbarStyle.FLOATING,
          )..show(context);
        } else {
          setState(() {
            loading = false;
          });
          print(response.body);
          return Flushbar(
            message: "SomeThing Went Wrong",
            duration: Duration(seconds: 3),
            flushbarPosition: FlushbarPosition.TOP,
            // flushbarStyle: FlushbarStyle.FLOATING,
          )..show(context);
        }
      } on SocketException catch (error) {
        setState(() {
          loading = false;
        });

        return Flushbar(
          message: error.toString(),
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } catch (e) {
        setState(() {
          loading = false;
        });
        return Flushbar(
          message: e.toString(),
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
        // print(e);
      }
    }
  }

  getOTP(context) async{
     signature = await SmsAutoFill().getAppSignature;
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
      String getMobile = myController.text.substring(myController.text.indexOf("")+3);
      print(getMobile);
     
      otp.sendOtp(getMobile,signature);
     
     
      setState(() {
        otpDetails = true;
      phoneNumber = getMobile;
      });
    } else {
      setState(() {
        
      });
    }
  }

  checkOTP(context) {
    setState(() {
      bool yesOrNo = otp.resultChecker(enteredOtp);
      print(yesOrNo);
      if (yesOrNo == true) {
otpDetails = false;
       _logIn();
      } else {
        Fluttertoast.showToast(
            msg: "Invalid Otp",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }
  void _listenOTP() async{
    await SmsAutoFill().listenForCode;
  }
  @override
  void initState() {
    _listenOTP();
    super.initState();
  }
 @override
  void dispose() {
    SmsAutoFill().unregisterListener();
    super.dispose();
   
}
  @override
  Widget build(BuildContext context) {
   
    bool yesOrNo = otp.resultChecker(enteredOtp);
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Form(
            key: _formkey,
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: Center(
                      child: Image.asset("assets/Logo.png"),
                    ),
                  ),
                  Text(
                    'Welcome Back',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'Sign in to continue',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  Padding(padding:EdgeInsets.only(left: 18.0, right: 18.0,top: 18.0),
                 
                  child: PhoneFieldHint(controller: myController,
                    
                  ), ),
                  // Padding(
                  //   padding: EdgeInsets.only(left: 18.0, right: 18.0),
                  //   child: TextFormField(
                  //     keyboardType: TextInputType.phone,
                  //     decoration: InputDecoration(hintText: "Mobile Number"),
                  //     validator: (value) => value.length < 10
                  //         ? 'Enter Valid Mobile Number'
                  //         : null,
                  //     onSaved: (value) => phoneNumber = value,
                  //   ),
                  // ),
                  otpDetails == true ? Padding(
                    padding: const EdgeInsets.only(left:18.0,right: 18.0),
                    child: PinFieldAutoFill(

                decoration: UnderlineDecoration(
                    
                    textStyle: TextStyle(fontSize: 20, color: Colors.black),
                    colorBuilder: FixedColorBuilder(Colors.green.withOpacity(0.3)),
                ),
                currentCode: _code,

                onCodeSubmitted: (code) {},
                onCodeChanged: (code) {
                    if (code.length == 4) {
                      FocusScope.of(context).requestFocus(FocusNode());
                      enteredOtp = int.parse(code);
                    }
                },
              ),
                  )
                      // ? Padding(
                      //     padding: EdgeInsets.only(left: 18.0, right: 18.0),
                      //     child: TextFormField(
                      //       onChanged: (val) {
                      //         enteredOtp = int.parse(val);
                      //       },
                      //       keyboardType: TextInputType.phone,
                      //       decoration: InputDecoration(hintText: "OTP"),
                      //       validator: (value) =>
                      //           value.length < 4 ? 'Enter Valid OTP' : null,
                      //     ),
                      //   )
                      : Container(),
                  otpDetails == false
                      ? Padding(
                          padding: EdgeInsets.only(top: 28.0),
                          child: MyRaisedButton(
                            onPressed: getOTP,
                            title: "Get OTP",
                            loading: loading,
                            textColor: Colors.white,
                            buttonColor: Theme.of(context).accentColor,
                          ),
                        )
                      : Padding(
                          padding: EdgeInsets.only(top: 28.0),
                          child: MyRaisedButton(
                            onPressed: checkOTP,
                            title: "Continue",
                            loading: loading,
                            textColor: Colors.white,
                            buttonColor: Theme.of(context).accentColor,
                          ),
                        )
                ],
              ),
            ),
          ),
        ));
  }
}
