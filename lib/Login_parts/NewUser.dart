import 'package:agri/Login_parts/login.dart';
import 'package:agri/utils/raised_button.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:wc_form_validators/wc_form_validators.dart';

class RegisterUser extends StatefulWidget {


  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  bool loading = false;

  bool passWordVisible = true;

  void togglePasswordVisiblity() {
    setState(() {
      passWordVisible = !passWordVisible;
    });
  }

String _phoneNumber;
String _firstName;
String _lastName;
String _email;
String _password;
String _password2;

Future<void> _logIn(context) async {
     String url = "https://api-agriculture.datavivservers.in/CreateUserRegister/";
    if (_formkey.currentState.validate()) {
      setState(() {
        loading = true;
      });

      _formkey.currentState.save();

      
         Map data ={
            "mob_no":_phoneNumber,
            "password":_password,
            "password2":_password2,
            "first_name":_firstName,
            "last_name":_lastName,
            "email":_email




          };
          var response = await http.post("$url",body: data,);
          var jsonObject = json.decode(response.body);
          if(response.statusCode==200){
            
            print(jsonObject);
            setState(() {
          loading = false;
        });
         
 Flushbar(
          message: "Account Added Sucessfully",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
            Navigator.pop(context);

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
          }else{
setState(() {
          loading = false;
        });
            return Flushbar(
          message: response.body,
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);

          }
       
       
      
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
        body: Form(
      key: _formkey,
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              
              Padding(
                padding: const EdgeInsets.only(top:38.0),
                child: Text(
        'Create Account',
        style: TextStyle(
          fontSize: 25.0,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).primaryColor,
        ),
                ),
              ),
              Row(
                children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top:18.0,left:28.0,right: 28.0),
            child: TextFormField(
              decoration: InputDecoration(hintText: "First Name"),
              validator: (value)=>value.isEmpty? "first Name is Required" : null,
              onSaved: (value)=> _firstName = value,

            ),
          )),
          Expanded(child: 
          Padding(
            padding: const EdgeInsets.only(top:18.0,left:28.0,right: 28.0),
            child: TextFormField(
              decoration: InputDecoration(hintText: "Last Name"),
               validator: (value)=>value.isEmpty? "first Name is Required" : null,
              onSaved: (value)=> _lastName = value,
            ),
          ))
                ],
              ),
                 Padding(
                padding: EdgeInsets.only(top:28.0,left: 18.0, right: 18.0),
                child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(hintText: "Email Id"),
         validator: (value)=> value.isEmpty? 'Email Id is Required' : null,
              onSaved: (value) => _email = value,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 28.0,left: 18.0, right: 18.0),
                child: TextFormField(
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(hintText: "PhoneNumber"),
         validator: (value)=> value.length < 10 ? 'Enter the Valid Phone Number' : null,
              onSaved: (value) => _phoneNumber = value,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 28.0, left: 18.0, right: 18.0),
                child: TextFormField(
        obscureText: true,
        decoration: InputDecoration(hintText: "Password"),
        validator: 
           Validators.compose([
             Validators.required('Password is required'),
             Validators.patternString(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$', 'Invalid Password\n Example Password: Test@123')
           ]),
        onSaved: (value) => _password = value,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 28.0, left: 18.0, right: 18.0),
                child: TextFormField(
        obscureText: true,
        decoration: InputDecoration(hintText: "Confirm Password"),
        validator: 
           Validators.compose([
             Validators.required('Password is required'),
             Validators.patternString(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$', 'Invalid Password\n Example Password: Test@123')
           ]),
        onSaved: (value) => _password2 = value,
                ),
              ),
               
              Padding(
                padding: EdgeInsets.only(top: 28.0),
                child: MyRaisedButton(
        onPressed: _logIn,
        title: "Sign Up",
        loading: loading,
        textColor: Colors.white,
        buttonColor: Theme.of(context).accentColor,
                ),
              ),
              
              Expanded(child: Container(),),
              Padding(
                padding: const EdgeInsets.only(bottom:18.0),
                child: RichText(
        text: TextSpan(
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
            ),
            children: [
              TextSpan(
                text: 'Already have an account?',
              ),
              TextSpan(
                text: "Sign In",
                style:
                    TextStyle(color: Theme.of(context).accentColor),
              )
            ]),
                )
              )

         
            ],
          ),
      ),
    ));
  }
}