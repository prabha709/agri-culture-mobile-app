import 'dart:io';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class ProductCardComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

Future getProducts()async{

  String url ="https://fab-agri-api.herokuapp.com/HomePageContentAPI/";
  try {
        var response = await http.get(
          "$url",
        );
        var jsonObject = json.decode(utf8.decode(response.bodyBytes));
        if (response.statusCode == 200) {
          
          return jsonObject;
        } else {
          return Flushbar(
            message: "Something Went wrong Please try again later",

            duration: Duration(seconds: 3),
            flushbarPosition: FlushbarPosition.TOP,
            // flushbarStyle: FlushbarStyle.FLOATING,
          )..show(context);
        }
      } on SocketException catch (error) {
        return Flushbar(
          message: "${error.message}",

          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      } catch (error) {
        return Flushbar(
          message: "${error.message}",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
          // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }

}

    return FutureBuilder(
      future: getProducts(),
      builder: (context, snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          if(snapshot.data == null){
            return Center(
             child:Text("Currently no products available"),
           );
          }else{
            return Container(
        // height: 260,
        // width: 170,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: Color(0xffc4c4c4),
            )),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.all(8),
        child: Column(
          children: [
            //Product Image
            Expanded(
              child: Image(
                image: NetworkImage(snapshot.data ),
                     ),
            ),

            //Product name
            Text(
              'Peas Seeds',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),

            //Product Price
            Row(
              children: [
                //orignal price
                Text(
                  '₹ 250',
                  style: TextStyle(decoration: TextDecoration.lineThrough),
                ),
                SizedBox(width: 8),
                //offer price
                Text(
                  '₹ 200/1 kg',
                  style: TextStyle(
                    decoration: TextDecoration.lineThrough,
                    color: Theme.of(context).primaryColor,
                  ),
                )
              ],
            ),

            //add To Cart button
            FlatButton(
              onPressed: () {},
              color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                'ADD TO CART',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      );
          }
        }else if(snapshot.hasError){
          return Center(
           child: Text("Internal Server Error"),
         );

        }else{
          return Center(
              child: SpinKitChasingDots(
            color: Color(0xff9DCF9C),
          ));
        }

      }

    );
    
  }
}
