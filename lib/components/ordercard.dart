import 'package:flutter/material.dart';

class OrderCardComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          //Body
          Row(
            children: [
              //Product Image
              Image(
                image: NetworkImage(
                  'https://cdn.shopify.com/s/files/1/0011/2341/8172/products/BN147_Red_Kidney_Beans_1_fb787624-d0ab-44ad-92a3-2ef7673764db_1024x1024.jpg?v=1523418046',
                ),
                height: 80,
              ),

              //Product Details
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Product Name
                    Text('Kidney Seeds'),

                    //Product Qty
                    Text(
                      'Qty 2kg',
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                      ),
                    ),

                    //Order Date
                    Text(
                      'Ordered on 16-oct-2019',
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
          //Order Again Button
        ],
      ),
    );
  }
}
