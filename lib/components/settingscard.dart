import 'package:flutter/material.dart';

class SettingCardComponent extends StatelessWidget {
  final String title;
  final String subtitle;
  final String navigatorScreenName;
  final Function onTap;
  const SettingCardComponent({
    Key key,
    @required this.title,
    @required this.navigatorScreenName,
    @required this.subtitle,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(height: 8),
          Divider(
            color: Colors.grey,
          ),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, navigatorScreenName),
            child: Container(
              alignment: Alignment.centerRight,
              child: Text(
                subtitle,
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
