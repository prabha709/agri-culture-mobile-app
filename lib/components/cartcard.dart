import 'package:flutter/material.dart';

class CartCardComponent extends StatelessWidget {
  final String productImageLink;
  final int quantity;
  final int price;
  CartCardComponent({
    @required this.price,
    @required this.productImageLink,
    @required this.quantity,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 115,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.grey),
          ),
        ),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //Product Image
            Image(
              image: NetworkImage(productImageLink),
            ),

            //Quantity
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //-1 from total
                FlatButton(
                  padding: EdgeInsets.zero,
                  onPressed: () {},
                  shape: CircleBorder(
                    side: BorderSide(color: Colors.grey),
                  ),
                  child: Text('-'),
                ),

                //Current Total Quantity of the product
                Text('1'),

                //+1 from total
                FlatButton(
                  onPressed: () {},
                  shape: CircleBorder(
                    side: BorderSide(color: Colors.grey),
                  ),
                  child: Text('+'),
                ),
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //Price
                Text('₹ 50'),
                SizedBox(width: 2),

                //Button
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      'X',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
