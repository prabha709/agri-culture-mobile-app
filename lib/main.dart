import 'package:agri/Login_parts/OtpVerification.dart';
import 'package:agri/Login_parts/login.dart';
import 'package:agri/screens/main.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';  

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}
class  MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     return MaterialApp(

      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Color(0xff9DCF9C),
        accentColor: Color(0xff50A94E),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),


      home: MyHomePage()
      // initialRoute: 'login',
      // onGenerateRoute: RouteGenerator.generateRoute,
    );
      
    
  }
}
class MyHomePage extends StatefulWidget {
  
  // This widget is the root of your application.
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

    String tokens;
    
 @override
  void initState() {
    _isUserLoggedIn();
    
      super.initState();
   
  }
_isUserLoggedIn()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
  var token = prefs.getString('token');
  print(token);
setState(() {
  tokens = token;
});
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
resizeToAvoidBottomInset: false,

     body:  tokens != null ?MainScreen(userToken:json.decode(tokens)):LoginScreen(),

    );
  }
}
